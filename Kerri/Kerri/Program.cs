﻿using System;
using System.Collections.Generic;
using Kerri.Bll.Entities.Users;
using Kerri.Dal;
using Kerri.Services;

namespace Kerri
{
    class Program
    {
        static void Main(string[] args)
        {
            KerriContext context = new KerriContext();
            IRepository<User> userRepo = new Repository<User>(context);
            User usr = new User() { Id = SequentialGuidUtils.CreateGuid(), Username = "Evgeniy1", Email = "qq" };
            userRepo.Insert(usr);
            IEnumerable<User> list = userRepo.GetAll();
            foreach (var user in list)
            {
                Console.WriteLine(user.Username);
                Console.WriteLine(user.Id);
            }

            
            Console.ReadKey();
        }
    }
}
