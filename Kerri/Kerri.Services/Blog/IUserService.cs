﻿using System;
using System.Collections.Generic;
using Kerri.Bll.Entities.Users;

namespace Kerri.Services.Blog
{
   public interface IUserService
   {
       void InsertUser(User user);
       void UpdateUser(User user);
       User GetUserById(Guid userId);
       IEnumerable<User> GetAllUsers();
   }
}
