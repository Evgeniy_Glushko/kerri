﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kerri.Bll;
using Kerri.Bll.Entities.Blog;
using Kerri.Dal;

namespace Kerri.Services.Blog
{
    public class BlogService:IBlogService
    {
        private readonly IRepository<BlogPost> _blogPostRepository;
        private readonly IRepository<BlogComment> _blogCommentRepository;
        public BlogService(IRepository<BlogPost> blogPostRepository,
            IRepository<BlogComment> blogCommentRepository)
        {
            _blogPostRepository = blogPostRepository;
            _blogCommentRepository = blogCommentRepository;
        }
        public void DeleteBlogPost(BlogPost blogPost)
        {
            if (blogPost == null)
                throw new ArgumentNullException(nameof(blogPost));

            _blogPostRepository.Delete(blogPost);
        }
        public BlogPost GetBlogPostById(Guid blogPostId)
        {
            return _blogPostRepository.GetById(blogPostId);
        }
        public IPagedList<BlogPost> GetAllBlogPosts(  DateTime? dateFrom = null, DateTime? dateTo = null,
            int pageIndex = 0, int pageSize = int.MaxValue)
        {
            var query = _blogPostRepository.GetAll();
            if (dateFrom.HasValue)
                query = query.Where(b => dateFrom.Value <= b.CreateDate);
            if (dateTo.HasValue)
                query = query.Where(b => dateTo.Value >= b.CreateDate);
            query = query.OrderByDescending(b => b.CreateDate);

            var blogPosts = new PagedList<BlogPost>(query.ToArray(), pageIndex, pageSize);
            return blogPosts;
        }
        public void InsertBlogPost(BlogPost blogPost)
        {
            if (blogPost == null)
                throw new ArgumentNullException(nameof(blogPost));

            _blogPostRepository.Insert(blogPost);
        }
        public void UpdateBlogPost(BlogPost blogPost)
        {
            if (blogPost == null)
                throw new ArgumentNullException(nameof(blogPost));
            _blogPostRepository.Update(blogPost);
        }
        public IList<BlogComment> GetAllComments(Guid userId)
        {
            return _blogCommentRepository.GetAll().Where(c => c.UserId == userId).OrderBy(c => c.CreateDate).ToList();
        }
        public BlogComment GetBlogCommentById(Guid blogCommentId)
        {
            return _blogCommentRepository.GetById(blogCommentId);
        }
        public IList<BlogComment> GetBlogCommentsByIds(Guid[] commentIds)
        {
            if (commentIds == null || commentIds.Length == 0)
                return new List<BlogComment>();
            return _blogCommentRepository.GetAll().Where(x => commentIds.Contains(x.Id)).OrderBy(x => x.CreateDate).ToList();
        }
        public void DeleteBlogComment(BlogComment blogComment)
        {
            if (blogComment == null)
                throw new ArgumentNullException(nameof(blogComment));

            _blogCommentRepository.Delete(blogComment);
        }
    }
}

