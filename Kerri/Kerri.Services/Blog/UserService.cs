﻿using System;
using System.Collections.Generic;
using Kerri.Bll.Entities.Users;
using Kerri.Dal;

namespace Kerri.Services.Blog
{
   public class UserService:IUserService
   {
       IRepository<User> _userRepo;

       public UserService(IRepository<User> userRepo )
       {
           _userRepo = userRepo;
       }

        public void InsertUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            _userRepo.Insert(user);
        }

        public void UpdateUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));
            _userRepo.Update(user);
        }
        public User GetUserById(Guid userId)
        {
            return _userRepo.GetById(userId);
        }
        public void DeleteUser(User user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user));

            _userRepo.Delete(user);
        }

       public IEnumerable<User> GetAllUsers()
       {
           return _userRepo.GetAll();
       }
   }
}
