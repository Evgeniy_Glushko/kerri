﻿using System;
using System.Collections.Generic;
using Kerri.Bll;
using Kerri.Bll.Entities.Blog;

namespace Kerri.Services.Blog
{
    public interface IBlogService
    {
        void DeleteBlogPost(BlogPost blogPost);
        BlogPost GetBlogPostById(Guid blogPostId);

        IPagedList<BlogPost> GetAllBlogPosts(DateTime? dateFrom = null, DateTime? dateTo = null,
            int pageIndex = 0, int pageSize = int.MaxValue);

        void InsertBlogPost(BlogPost blogPost);
        void UpdateBlogPost(BlogPost blogPost);
        IList<BlogComment> GetAllComments(Guid userId);
        BlogComment GetBlogCommentById(Guid blogCommentId);
        IList<BlogComment> GetBlogCommentsByIds(Guid[] commentIds);
        void DeleteBlogComment(BlogComment blogComment);
    }
}
