﻿using Kerri.Bll.Entities.Users;

namespace Kerri.Dal.Mapping.Users
{
    public class UserMap : KerriEntityTypeConfiguration<User>
    {
        public UserMap()
        {
            ToTable("User");
            HasKey(c => c.Id);
            Property(u => u.Username).HasMaxLength(1000).IsOptional();
            Property(u => u.Email).HasMaxLength(1000);
            Property(u => u.LastName).HasMaxLength(20);
            Property(u => u.AvatarLink).HasMaxLength(1000);
            Property(u => u.RoleId);
            Ignore(p => p.IsOnline);
        }
    }
}
