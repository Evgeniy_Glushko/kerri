﻿using System.Data.Entity.ModelConfiguration;

namespace Kerri.Dal.Mapping
{
    public abstract class KerriEntityTypeConfiguration<T> : EntityTypeConfiguration<T> where T : class
    {
        protected KerriEntityTypeConfiguration()
        {
            PostInitialize();
        }
        protected virtual void PostInitialize()
        {

        }
    }
}
