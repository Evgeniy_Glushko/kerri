﻿using Kerri.Bll.Entities.Blog;

namespace Kerri.Dal.Mapping.Blog
{
    public class BlogPostMap : KerriEntityTypeConfiguration<BlogPost>
    {
        public BlogPostMap()
        {
            ToTable("BlogPost");
            HasKey(bp => bp.Id);
            Property(bp => bp.Title).IsRequired();
            Property(bp => bp.Body).IsRequired();
            Property(bp => bp.AllowComments).IsOptional();
            Property(bp => bp.CommentCount).IsOptional();
            Property(bp => bp.UserId).IsOptional();

        }
    }
}
