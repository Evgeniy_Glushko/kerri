﻿using Kerri.Bll.Entities.Blog;

namespace Kerri.Dal.Mapping.Blog
{
    public class BlogCommentMap : KerriEntityTypeConfiguration<BlogComment>
    {
        public BlogCommentMap()
        {
            ToTable("BlogComment");
            HasKey(pr => pr.Id);

            HasRequired(bc => bc.BlogPost)
                .WithMany(bp => bp.BlogComments)
                .HasForeignKey(bc => bc.BlogPostId);

            HasRequired(cc => cc.User)
                .WithMany()
                .HasForeignKey(cc => cc.UserId);
        }
    }
}
