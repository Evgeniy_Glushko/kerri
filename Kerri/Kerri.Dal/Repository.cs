﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Kerri.Bll.Entities;

namespace Kerri.Dal
{
    public sealed class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly IDbContext _context;
        private IDbSet<T> _entities;
        public Repository(IDbContext context)
        {
            _context = context;
        }
        public IEnumerable<T> GetAll()
        {
            return Entities.ToList();
        }
        public T GetById(Guid id)
        {
            return Entities.Find(id);
        }
        public void Insert(T entity)
        {
            Entities.Add(entity);
            _context.SaveChanges();
        }
        public void Insert(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
                Entities.Add(entity);
            _context.SaveChanges();
        }
        public void Update(T entity)
        {
            _context.SaveChanges();
        }
        public void Update(IEnumerable<T> entities)
        {
            this._context.SaveChanges();
        }
        public void Delete(T entity)
        {
            Entities.Remove(entity);
            _context.SaveChanges();
        }
        public void Delete(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
                Entities.Remove(entity);
            _context.SaveChanges();
        }
        private IDbSet<T> Entities
        {
            get
            {
                if (_entities == null)
                    _entities = _context.Set<T>();
                return _entities;
            }
        }
    }
}
