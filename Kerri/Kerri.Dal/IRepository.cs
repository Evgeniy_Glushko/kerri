﻿using System;
using System.Collections.Generic;
using Kerri.Bll.Entities;

namespace Kerri.Dal
{
    public interface IRepository<T> where T : BaseEntity
    {
        T GetById(Guid id);
        void Insert(T entity);
        void Insert(IEnumerable<T> entities);
        void Update(T entity);
        void Update(IEnumerable<T> entities);
        void Delete(T entity);
        void Delete(IEnumerable<T> entities);
        IEnumerable<T> GetAll();
    }
}
