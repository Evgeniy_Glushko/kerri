﻿using System.Data.Entity;
using Kerri.Bll.Entities;

namespace Kerri.Dal
{
   public interface IDbContext
   {
       IDbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity;
        int SaveChanges();
    }
}
