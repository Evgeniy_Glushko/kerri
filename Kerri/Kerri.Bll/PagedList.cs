﻿using System.Collections.Generic;
using System.Linq;

namespace Kerri.Bll
{
    public class PagedList<T> : List<T>, IPagedList<T>
    {
        public PagedList(ICollection<T> source, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            if (source != null)
            {
                TotalCount = source.Count;
                TotalPages = TotalCount / pageSize;

                if (TotalCount % pageSize > 0)
                    TotalPages++;
            }
            AddRange(source.Skip(pageIndex * pageSize).Take(pageSize).ToList());
        }

        public int PageIndex { get; }
        public int TotalCount { get; }
        public int TotalPages { get; }

        public bool HasPreviousPage => (PageIndex > 0);
        public bool HasNextPage => (PageIndex + 1 < TotalPages);
    }
}
