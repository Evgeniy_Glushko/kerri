﻿using System.Collections.Generic;

namespace Kerri.Bll
{
    public interface IPagedList<T>: IList<T>
    {
        int PageIndex { get; }
        int TotalCount { get; }
        int TotalPages { get; }
        bool HasPreviousPage { get; }
        bool HasNextPage { get; }
    }
}
