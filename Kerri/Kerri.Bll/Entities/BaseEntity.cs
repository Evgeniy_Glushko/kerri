﻿using System;

namespace Kerri.Bll.Entities
{
    public abstract class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
