﻿using System;
using System.Collections.Generic;
using Kerri.Bll.Entities.Users;

namespace Kerri.Bll.Entities.Blog
{
    public class BlogPost : BaseEntity
    {
        private ICollection<BlogComment> _blogComments;
        public string Title { get; set; }
        public string Body { get; set; }
        public bool AllowComments { get; set; }
        public int CommentCount { get; set; }
        public DateTime? CreateDate { get; set; }
        public Guid UserId { get; set; }
        public virtual User User { get; set; }

        public virtual ICollection<BlogComment> BlogComments
        {
            get { return _blogComments ?? (_blogComments = new List<BlogComment>()); }
            protected set { _blogComments = value; }
        }
    }
}
