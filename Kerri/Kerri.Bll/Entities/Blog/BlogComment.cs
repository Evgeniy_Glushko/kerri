﻿using System;
using Kerri.Bll.Entities.Users;

namespace Kerri.Bll.Entities.Blog
{
    public class BlogComment : BaseEntity
    {
        public Guid UserId { get; set; }
        public string CommentText { get; set; }
        public Guid BlogPostId { get; set; }
        public DateTime CreateDate { get; set; }
        public virtual User User { get; set; }
        public virtual BlogPost BlogPost { get; set; }
    }
}
