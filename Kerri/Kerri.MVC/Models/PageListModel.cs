﻿using System.Collections.Generic;

namespace Kerri.MVC.Models
{
    public class PageListModel
    {
        public IEnumerable<BlogPostModel> Posts { get; set; }

        public int PageIndex { get; set; }
        public int TotalCount { get; set; }
        public int TotalPages { get; set; }
        public bool HasPreviousPage => (PageIndex > 0);
        public bool HasNextPage => (PageIndex + 1 < TotalPages);
    }
}