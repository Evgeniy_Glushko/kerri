﻿using System;

namespace Kerri.MVC.Models
{
    public class BlogCommentModel
    {
        public Guid UserId { get; set; }
        public string CommentText { get; set; }
        public Guid BlogPostId { get; set; }
        public DateTime CreateDate { get; set; }
        public virtual UserModel User { get; set; }
        public virtual BlogPostModel BlogPost { get; set; }
    }
}