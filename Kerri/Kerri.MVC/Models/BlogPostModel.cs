﻿using System;
using System.Collections.Generic;

namespace Kerri.MVC.Models
{
    public class BlogPostModel
    {
        private ICollection<BlogCommentModel> _blogComments;
        public string Title { get; set; }
        public string Body { get; set; }
        public bool AllowComments { get; set; }
        public int CommentCount { get; set; }
        public DateTime? CreateDate { get; set; }
        public Guid UserId { get; set; }
        public virtual UserModel User { get; set; }

        public virtual ICollection<BlogCommentModel> BlogComments
        {
            get { return _blogComments ?? (_blogComments = new List<BlogCommentModel>()); }
            protected set { _blogComments = value; }
        }
    }
}