﻿namespace Kerri.MVC.Models
{
    public class UserModel
    {
        public string Username { get; set; }
        public string LastName { get; set; }
        public string AvatarLink { get; set; }
        public string Email { get; set; }
        public string RoleId { get; set; }
        public string Password { get; set; }
        public bool IsOnline { get; set; }
    }
}