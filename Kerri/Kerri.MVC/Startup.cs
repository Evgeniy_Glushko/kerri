﻿using Kerri.MVC.Mapping;
using Kerri.MVC.Util;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Kerri.MVC.Startup))]
namespace Kerri.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AutofacConfig.ConfigureContainer();
            AutoMapperConfig.RegisterMappings();
            ConfigureAuth(app);
        }
    }
}
