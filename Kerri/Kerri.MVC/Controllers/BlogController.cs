﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using Kerri.Bll;
using Kerri.Bll.Entities.Blog;
using Kerri.Bll.Entities.Users;
using Kerri.MVC.Models;
using Kerri.Services;
using Kerri.Services.Blog;

namespace Kerri.MVC.Controllers
{
    public class BlogController : Controller
    {
        // GET: Blog
        //public ActionResult Index()
        //{
        //    return View();
        //}
        private const int PostsPerPageDefaultValue = 5;
        private IBlogService _blogSrv;
        private IUserService _userSrv;
        public BlogController(IBlogService blogSrv, IUserService userSrv)
        {
            _blogSrv = blogSrv;
            _userSrv = userSrv;
        }
        //public string Index()
        //{
        //    User usr = new User() { Id = SequentialGuidUtils.CreateGuid(), Username = "Evgeniy1", Email = "qq" };

        //    BlogPost post = new BlogPost() {Id = SequentialGuidUtils.CreateGuid(), Title = "New", Body = "Life is Good", User = usr};
        //    _blogSrv.InsertBlogPost(post);
        //   // IPagedList<BlogPost> list = _blogSrv.GetAllBlogPosts();
        //    // List<BlogPost> listPosts = list.ToList();
        //    var list = _blogSrv.GetAllBlogPosts();
        //    var model = new PageListModel()
        //    {
        //        PageIndex = list.PageIndex,
        //        TotalPages = list.TotalPages,
        //        Posts = Mapper.Map<IEnumerable<BlogPost>, IEnumerable<BlogPostModel>>(list),
        //        TotalCount = list.TotalCount
        //    };
        //    return model.Posts.First().Body;
        //}
        public ActionResult Index(int page = 1, int perPage = PostsPerPageDefaultValue)
        {
            //var pagesCount = (int)Math.Ceiling(_blogSrv.GetAllBlogPosts().TotalCount / (decimal)perPage);

            //var posts = this.blogPostsData
            //    .All()
            //    .Where(x => !x.IsDeleted)
            //    .OrderByDescending(x => x.CreatedOn)
            //    .Project().To<BlogPostAnnotationViewModel>()
            //    .Skip(perPage * (page - 1))
            //    .Take(perPage);

            //var model = new IndexViewModel
            //{
            //    Posts = posts.ToList(),
            //    CurrentPage = page,
            //    PagesCount = pagesCount,
            //};
            var list = _blogSrv.GetAllBlogPosts();
            var model = new PageListModel()
            {
                PageIndex = list.PageIndex,
                TotalPages = list.TotalPages,
                Posts = Mapper.Map<IEnumerable<BlogPost>, IEnumerable<BlogPostModel>>(list),
                TotalCount = list.TotalCount
            };
            return this.View(model);
        }
    }
}