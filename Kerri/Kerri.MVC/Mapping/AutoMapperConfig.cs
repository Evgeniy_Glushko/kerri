﻿using AutoMapper;
using Kerri.Bll;
using Kerri.Bll.Entities.Blog;
using Kerri.Bll.Entities.Users;
using Kerri.MVC.Models;

namespace Kerri.MVC.Mapping
{
    public class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.CreateMap<User, UserModel>();
            Mapper.CreateMap<BlogPost, BlogPostModel>();
            Mapper.CreateMap<BlogComment, BlogCommentModel>();
            Mapper.CreateMap<PagedList<BlogPost>, PageListModel>();
            



        }
    }
}